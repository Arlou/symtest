<?php

namespace AppBundle\Controller\Admin\Articles;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Component\Pager\Paginator;


class ManageController implements PaginatorAwareInterface
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var Paginator
     */
    private $paginator;

    public function __construct(EngineInterface $templating, EntityManager $entityManager, Paginator $paginator)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->setPaginator($paginator);
    }

    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @param Request $request
     * @param int $defaultPage
     * @param int $itemsPerPage
     * @return Response
     */
    public function indexAction(Request $request, $defaultPage = 1, $itemsPerPage = 50)
    {
        $pagination = $this->paginator->paginate(
            $this->entityManager->createQuery('SELECT a FROM AppBundle:Article a ORDER BY a.date_created DESC'),
            $request->query->getInt('page', $defaultPage),
            $itemsPerPage
        );

        return $this->templating->renderResponse('admin/articles/manage_articles.html.twig', ['pagination' => $pagination]);
    }
}
