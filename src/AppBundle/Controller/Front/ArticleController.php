<?php

namespace AppBundle\Controller\Front;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EngineInterface $templating, EntityManager $entityManager)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    public function indexAction($id)
    {
        /** @var Article $article */
        $article = $this->entityManager->getRepository('AppBundle:Article')->find($id);

        if (empty($article))
        {
            throw new NotFoundHttpException('No article found!');
        }

        return $this->templating->renderResponse('front/article.html.twig', ['article' => $article]);
    }
}
