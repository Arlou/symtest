<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Interfaces\TempFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="article")
 * @ORM\HasLifecycleCallbacks
 */
class Article implements TempFile
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $text = '';

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $image_filename = null;

    /**
     * @Assert\Type("\DateTime")
     *
     * @ORM\Column(type="datetime")
     */
    protected $date_created;

    /**
     * @Assert\Image(
     *     maxSize = "5M",
     *     maxRatio = "100",
     *     minRatio = "0.01",
     *     maxWidth = "10000",
     *     maxHeight = "10000"
     * )
     */
    private $file = null;
    private $temp = null;
    private $tempFileName = null;

    #region Image handlers
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->image_filename)) {
            // store the old name to delete after the update
            $this->temp = $this->image_filename;
            $this->image_filename = null;
        } else {
            $this->image_filename = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $this->image_filename = $this->generateFilename();;
        }
    }

    /**
     * Function generates filename, using file in $this->file attribute
     *
     * @return null|string
     */
    private function generateFilename()
    {
        if (empty($this->getFile()))
        {
            return null;
        }

        return sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->guessExtension();
    }

    public function handleTmpFile(UploadedFile $file)
    {
        $this->file = $file;
        $this->tempFileName = $this->generateFilename();
        $file->move($this->getTmpUploadRootDir(), $this->tempFileName);

        return $this->tempFileName;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->image_filename);

        // check if we have an old image
        if (!empty(trim($this->temp))) {
            // delete the old image
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->image_filename
            ? null
            : $this->getUploadRootDir() . '/' . $this->image_filename;
    }

    public function getWebPath()
    {
        return null === $this->image_filename
            ? null
            : $this->getUploadDir() . '/' . $this->image_filename;
    }

    public function getAbsoluteTmpPath($tempFileName = null, $dateFolder = null)
    {
        return empty($tempFileName) && empty($this->tempFileName)
            ? null
            : $this->getUploadRootDir() . '/tmp/' . ($dateFolder ?: date('Y-m-d')) . '/' . ($tempFileName ?: $this->tempFileName);
    }

    public function getWebTmpPath($tempFileName = null, $dateFolder = null)
    {
        return empty($tempFileName) && empty($this->tempFileName)
            ? null
            : $this->getUploadDir() . '/tmp/' . ($dateFolder ?: date('Y-m-d')) . '/' . ($tempFileName ?: $this->tempFileName);
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Function returns path to temporary directory, where temp images are located
     *
     * @param null $dateFolder
     * @return string
     */
    protected function getTmpUploadRootDir($dateFolder = null)
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getUploadDir(). '/' . 'tmp/' . ($dateFolder ?: date('Y-m-d'));
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images/articles';
    }

    public function deleteImage()
    {
        unlink($this->getAbsolutePath());
        $this->image_filename = null;
    }
    #endregion

    #region getters
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Article
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set imageFilename
     *
     * @param string $imageFilename
     *
     * @return Article
     */
    public function setImageFilename($imageFilename)
    {
        $this->image_filename = $imageFilename;

        return $this;
    }

    /**
     * Get imageFilename
     *
     * @return string
     */
    public function getImageFilename()
    {
        return $this->image_filename;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Article
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getTempFileName()
    {
        return $this->tempFileName;
    }

    public function setTempFileName($tempFileName)
    {
        $this->tempFileName = $tempFileName;

        return $this;
    }
    #endregion
}
