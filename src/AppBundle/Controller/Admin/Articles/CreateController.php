<?php

namespace AppBundle\Controller\Admin\Articles;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class CreateController
{
    // Template name for cookie in witch temporary image file is stored
    const TEMP_ARTICLE_IMAGE_COOKIE_NAME = 'tmp:action:%s:entity:%s';

    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EngineInterface $templating, EntityManager $entityManager, FormFactory $formFactory, Router $router)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    public function indexAction(Request $request)
    {
        $article = new Article();
        $form = $this->formFactory->createBuilder('form', $article)
            ->add('title', 'text')
            ->add('text', 'textarea', ['empty_data' => ''])
            ->add('file')
            ->add('save', 'submit', ['label' => 'Create Article'])
            ->add('saveAndAdd', 'submit', ['label' => 'Save and Add'])
            ->getForm();

        $form->handleRequest($request);

        $response = new Response();
        $tempImageCookieName = sprintf(self::TEMP_ARTICLE_IMAGE_COOKIE_NAME, 'create', 'article_image');
        $tempFilename = $request->cookies->get($tempImageCookieName);

        // If user deleted image or uploaded new image, we clear cookie of temporary file
        if ($request->request->get('deleteImage') || !empty($request->files->get('form')['file'])) {
            $response->headers->clearCookie($tempImageCookieName);
            $tempFilename = null;
        }

        if ($form->isValid()) {
            $article->setDateCreated(new \DateTime());

            // If new file has not been uploaded, and temp image, that has been uploaded earlier has not been deleted
            // we move temp image to regular folder and set image_filename on Article model
            if (empty($form->get('file')->getData()) && !$request->request->get('deleteImage') && !empty($tempFilename)) {
                $article->setImageFilename($tempFilename);
                if (file_exists($article->getAbsoluteTmpPath($tempFilename))) {
                    rename($article->getAbsoluteTmpPath($tempFilename), $article->getAbsolutePath());
                }
            }

            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? 'admin_articles_create'
                : 'admin_articles_manage';

            $response = new RedirectResponse($this->router->generate($nextAction));

            // We clear temp image cookie, because article was successfully saved
            $response->headers->clearCookie($tempImageCookieName);

            return $response;
        } elseif (!empty($form->get('file')->getData()) && $form->get('file')->isValid()) {
            // If  form is not valid, but image file is not empty and is valid, we save this file to temporary directory
            $tempFilename = $form->getData()->handleTmpFile($form->get('file')->getData());
            $response->headers->setCookie(new Cookie($tempImageCookieName, $tempFilename));
        }

        return $this->templating->renderResponse(
            'admin/articles/create_article.html.twig',
            [
                'form' => $form->createView(),
                'tempImagePath' => $form->getData()->getWebTmpPath($tempFilename)
            ],
            $response
        );
    }
}
