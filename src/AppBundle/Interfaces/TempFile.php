<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface for entities that have related files, that can be stored in temporary folder
 *
 * Interface TempFile
 * @package AppBundle\Interfaces
 */
interface TempFile
{
    /**
     * Function move recently uploaded file to temp folder and returns it's name
     *
     * @param UploadedFile $file
     * @return null|string
     */
    public function handleTmpFile(UploadedFile $file);

    public function getWebTmpPath($tempFilePath = null, $dateFolder = null);

    public function getAbsoluteTmpPath($tempFilePath = null, $dateFolder = null);
}