<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;
    /**
     * @var EngineInterface
     */
    private $templating;

    public function __construct(AuthenticationUtils $authenticationUtils, EngineInterface $templating)
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->templating = $templating;
    }

    public function loginAction()
    {
        return $this->templating->renderResponse('security/login.html.twig', [
            'error'         => $this->authenticationUtils->getLastAuthenticationError(),
            'last_username' => $this->authenticationUtils->getLastUsername(),
        ]);
    }
}