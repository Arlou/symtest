<?php

namespace AppBundle\Controller\Admin\Articles;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteController
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager, Router $router)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function indexAction($id)
    {
        $article = $this->entityManager->getRepository('AppBundle:Article')->find($id);

        if (empty($article))
        {
            throw new NotFoundHttpException('No article found!');
        }

        $this->entityManager->remove($article);
        $this->entityManager->flush();

        return new RedirectResponse($this->router->generate('admin_articles_manage'));
    }
}
