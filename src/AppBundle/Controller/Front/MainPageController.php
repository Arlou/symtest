<?php

namespace AppBundle\Controller\Front;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Component\Pager\Paginator;

class MainPageController implements PaginatorAwareInterface
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var Paginator
     */
    private $paginator;

    public function __construct(EngineInterface $templating, EntityManager $entityManager, Paginator $paginator)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->setPaginator($paginator);
    }

    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    public function indexAction(Request $request, $defaultPage = 1, $itemsPerPage = 10)
    {
        $pagination = $this->paginator->paginate(
            $this->entityManager->createQuery('SELECT a FROM AppBundle:Article a ORDER BY a.date_created DESC'),
            $this->getCurrentPage($request, $defaultPage, $itemsPerPage),
            $itemsPerPage
        );
        $pagination->setUsedRoute('main_page');
        $pagination->setParam('_from', null);

        return $this->templating->renderResponse('front/index.html.twig', ['pagination' => $pagination]);
    }

    /**
     * Function gets parameter "_from", if it exists in request.
     * Then it calculates on what page this article is located
     *
     * @param Request $request
     * @param $defaultPage
     * @param $itemsPerPage
     * @return int
     */
    private function getCurrentPage(Request $request, $defaultPage, $itemsPerPage)
    {
        $currentPage = null;

        if (!empty($fromId = $request->query->getInt('_from')))
        {
            $fromArticle = $this->entityManager->createQuery('SELECT a.date_created FROM AppBundle:Article a WHERE a.id = :id')
                ->setParameter(':id', $fromId)
                ->setMaxResults(1)
                ->getResult();

            if (!empty($fromArticle[0]['date_created'])) {

                $totalCount = $this->entityManager->createQuery('SELECT COUNT(a.id) FROM AppBundle:Article a WHERE a.date_created >= :fromDateCreated')
                    ->setParameter(':fromDateCreated', $fromArticle[0]['date_created'])
                    ->getSingleScalarResult();

                $currentPage = ceil($totalCount / max(1, $itemsPerPage));
            }
        }

        return $currentPage ?: $request->query->getInt('page', $defaultPage);
    }
}
