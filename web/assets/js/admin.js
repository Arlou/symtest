jQuery(function()
{
    var editArticleForm = jQuery('form.edit-article'),
        imageId;

    editArticleForm.on('click', '.delete-image', function()
    {
        imageId = jQuery(this).data('image-id');

        jQuery(this).fadeOut();
        editArticleForm.find('.admin-image-edit.image-id-' + imageId).fadeOut();
        editArticleForm.find('input[name="deleteImage"].image-id-' + imageId).val(true);
    });

    jQuery(document).on('click', '.need-confirm', function()
    {
        return confirm($(this).data('confirm-text') || 'Are you sure?');
    });
});