<?php

namespace AppBundle\Controller\Admin\Articles;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EditController
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EngineInterface $templating, EntityManager $entityManager, FormFactory $formFactory, Router $router)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    public function indexAction(Request $request, $id)
    {
        /** @var Article $article */
        $article = $this->entityManager->getRepository('AppBundle:Article')->find($id);

        if (empty($article))
        {
            throw new NotFoundHttpException('No article found!');
        }

        $form = $this->formFactory->createBuilder('form', $article)
            ->add('title', 'text')
            ->add('text', 'textarea')
            ->add('file')
            ->add('imagePath', 'hidden', [
                'data' => $article->getWebPath(),
                'mapped' => false,
            ])
            ->add('save', 'submit', ['label' => 'Save'])
            ->add('saveAndExit', 'submit', ['label' => 'Save and exit'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($request->request->get('deleteImage') && empty($request->files->get('form')['file']))
            {
                $article->deleteImage();
            }

            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $url = $form->get('saveAndExit')->isClicked()
                ? $this->router->generate('admin_articles_manage')
                : $this->router->generate('admin_articles_edit', ['id' => $id]);

            return new RedirectResponse($url);
        }

        return $this->templating->renderResponse('admin/articles/edit_article.html.twig', ['form' => $form->createView()]);
    }
}
